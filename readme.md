# Secure Facebook #

## Description ##

Secure Facebook is an minimal implementation of the social network website Facebook, only more secure thanks to its public key encryption scheme! The robust akka actor model message passing powers the core of the simulator and the incredibly smart Spray Can provides the restful APIs.

## List of APIs ##
* Home Page
* Wall Post
* Friend API
* Profile API
* Picture and Albums.

## Owners ##
* [Tarun Gupta Akirala](https://bitbucket.org/tarun_gupta_akirala/) 
* [Rakesh Dammalapati ](https://bitbucket.org/rakeshdrk/)


### What is working?  ###
All the above mentioned API's are working. 

### Largest user set tried on? ###
Created 10,000 users and performed varies API operations. Simulator is based on statistics provided [here](https://zephoria.com/top-15-valuable-facebook-statistics/)